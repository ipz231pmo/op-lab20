﻿#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main(){
	srand(time(0));
	int** matrix = (int**) malloc(sizeof(int*)*4);
	for (int i = 0; i < 4; i++)
		matrix[i] = (int*)malloc(sizeof(int) * 4);
	printf("matrix\n");
	for (int i = 0; i < 4; i++){
		for (int j = 0; j < 4; j++){
			matrix[i][j] = rand() % 201 - 100;
			printf("%4d ", matrix[i][j]);
		}
		printf("\n");
	}
	// Make Array
	int* minColumnElements = (int*)malloc(sizeof(int)*4);
	for (int j = 0; j < 4; j++) {
		int mini = 0;
		for (int i = 1; i < 4; i++) {
			if (matrix[i][j] < matrix[mini][j]) mini = i;
		}
		minColumnElements[j] = matrix[mini][j];
	}
	// Print Array
	printf("minColumnElements = {");
	for (int i = 0; i < 4; i++)
		printf("%4d ", minColumnElements[i]);
	printf("}\n");
	printf("minColumnElements adresses\n");
	for (int i = 0; i < 4; i++) 
		printf("%p\n", &minColumnElements[i]);
	// Free Memory
	for (int i = 0; i < 4; i++)
		free(matrix[i]);
	free(matrix);
	free(minColumnElements);
	return 0;
}