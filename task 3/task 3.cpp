﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <malloc.h>
int find(int* base, int size, int element) {
	for (int i = 0; i < size; i++)
		if (base[i] == element) return 1;
	return 0;
}
int main(){
	srand(time(0));
	int a, b;
	printf("Enter sizes of arrays x and y: ");
	scanf("%d %d", &a, &b);
	int 
		*x = (int*)malloc(sizeof(int)*a), 
		*y = (int*)malloc(sizeof(int)*b);
	printf("x = { ");
	for (int i = 0; i < a; i++){
		x[i] = rand() % 26;
		printf("%2d ", x[i]);
	}
	printf(" }\n");
	printf("y = { ");
	for (int i = 0; i < b; i++) {
		y[i] = rand() % 26;
		printf("%2d ", y[i]);
	}
	printf(" }\n");
	// Making new arrays
	int* combinedArray = (int*)malloc(sizeof(int) * (a + b)), *unionArray=NULL;
	for (int i = 0; i < a; i++)
			combinedArray[i] = x[i];
	for (int i = 0; i < b; i++)
		combinedArray[a + i] = y[i];
	int unionArraySize = 0;
	for (int i = 0; i < a; i++)
		if (find(y, b, x[i]) && !find(unionArray, unionArraySize, x[i])) {
			unionArraySize++;
			unionArray = (int*)realloc(unionArray, sizeof(int) * unionArraySize);
			unionArray[unionArraySize - 1] = x[i];
		}
	// Print new arrays
	printf("Combined array = { ");
	for (int i = 0; i < a + b; i++)
		printf("%2d ", combinedArray[i]);
	printf("}\n");
	printf("Union array = { ");
	for (int i = 0; i < unionArraySize; i++)
		printf("%2d ", unionArray[i]);
	printf("}\n");
	free(x); free(y);
	free(combinedArray); free(unionArray);
	return 0;
}