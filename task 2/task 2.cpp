﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <time.h>
int main() {
    srand(time(0));
    int numRows, numCols;
    printf("Enter number of rows and cols: ");
    scanf("%d %d", &numRows, &numCols);
    int** array;
    array = (int**)malloc(sizeof(int*) * numRows);
    for (int i = 0; i < numRows; i++) array[i] = (int*)malloc(sizeof(int) * numCols);
    // Randomize and print
    printf("array\n");
    for (int i = 0; i < numRows; i++) {
        for (int j = 0; j < numCols; j++) {
            array[i][j] = rand() % 201 - 100;
            printf("%4d ", array[i][j]);
        }
        printf("\n");
    }
    // Delete Col
    int colToDelete;
    printf("Enter number of col to delete: ");
    scanf("%d", &colToDelete);
    colToDelete--;
    for (int i = 0; i < numRows; i++) 
        for (int j = colToDelete; j < numCols - 1; j++) 
            array[i][j] = array[i][j + 1];
    numCols--;
    for (int i = 0; i < numRows; i++)
        array[i] = (int*)realloc(array[i], sizeof(int) * numCols);
    // Print
    printf("array after deleting %d col\n", colToDelete+1);
    for (int i = 0; i < numRows; i++) {
        for (int j = 0; j < numCols; j++) 
            printf("%4d ", array[i][j]);
        printf("\n");
    }
    // Delete row
    int minRowIndex = 0, minColIndex = 0;
    for (int i = 0; i < numRows; i++)
        for (int j = 0; j < numCols; j++)
            if (array[i][j] < array[minRowIndex][minColIndex]) {
                minRowIndex = i;
                minColIndex = j;
            }
    for (int j = 0; j < numCols; j++)
        for (int i = minRowIndex; i < numRows - 1; i++) 
            array[i][j] = array[i + 1][j];
    free(array[numRows - 1]);
    numRows--;
    array = (int**)realloc(array, sizeof(int*)*numRows);
    // Print
    printf("array after deleting the row with the least element\n");
    for (int i = 0; i < numRows; i++) {
        for (int j = 0; j < numCols; j++)
            printf("%4d ", array[i][j]);
        printf("\n");
    }
    // Free memory
    for (int i = 0; i < numRows; i++)
        free(array[i]);
    free(array);
    return 0;
}